﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.Design.Widget;
using Android.Support.V4.View;
using Android.Support.V4.Widget;
using Android.Support.V7.App;
using Android.Support.V7.View.Menu;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using MvvmCross.Droid.Support.V7.AppCompat;
using MvvmCross.Droid.Support.V7.RecyclerView;
using MvvmCross.Platforms.Android.Binding.BindingContext;
using MvvmCross.Platforms.Android.Views;
using MvvmCross.WeakSubscription;
using MyToDayApp.Core.ViewModels;
using MyToDayApp.Droid.Views.Adapters;

namespace MyToDayApp.Droid.Views
{
    [Activity(Theme = "@style/Theme.DesignDemo")]
    public class TaskListView:MvxAppCompatActivity<TaskListViewModel >
    {
        MvxNotifyPropertyChangedEventSubscription _token;
        public MvxRecyclerView _rvToDoListView;
        public ImageButton _btnCompleteTask;
        private Android.Support.V7.Widget.Toolbar _toolbar;
        FloatingActionButton _btnAdd;
        public Android.Support.V7.Widget.SearchView _searchView;
        TaskListItemAdapter adapter;
        DrawerLayout drawerLayout;
        NavigationView navigationView;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.task_list_view);
            GetReference();
            SetupToolBar();
            RegisterHandlers();
            adapter = new TaskListItemAdapter((IMvxAndroidBindingContext)BindingContext, this.ViewModel, this);
            _rvToDoListView.Adapter = adapter;
            ViewModel.GetData();
            adapter.NotifyDataSetChanged();


            _token = ViewModel.WeakSubscribe(ViewModelPropertyChanged);
            var drawerToggle = new ActionBarDrawerToggle(this, drawerLayout, _toolbar, Resource.String.drawer_open, Resource.String.drawer_close);
            drawerLayout.SetDrawerListener(drawerToggle);
            drawerToggle.SyncState();
            setupDrawerContent(navigationView);

        }


      

        void setupDrawerContent(NavigationView navigationView)
        {
            navigationView.NavigationItemSelected += (sender, e) =>
            {
                e.MenuItem.SetChecked(true);
                drawerLayout.CloseDrawers();
            };
        }
        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            navigationView.InflateMenu(Resource.Menu.menu);
            MenuInflater menuInflater = new MenuInflater(this);
            MenuInflater.Inflate(Resource.Menu.toolbar_menu,menu);
            var searchItem = menu.FindItem(Resource.Id.appSearchBar);
            _searchView = (Android.Support.V7.Widget.SearchView)searchItem.ActionView;
            _searchView.QueryTextChange += SearchView_QueryTextChange;
            return true;
        }

        private void SearchView_QueryTextChange(object sender, Android.Support.V7.Widget.SearchView.QueryTextChangeEventArgs e)
        {
            ViewModel.SearchQuery(_searchView.Query);
        }

        private void ViewModelPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "MytodoList")
            {
                adapter.NotifyDataSetChanged();
            }
         
        }

        private void RegisterHandlers()
        {
            //_btnAdd.Click += _btnAdd_Click;
            _btnCompleteTask.Click += _btnCompleteTask_Click;
               
        }

        private void _btnCompleteTask_Click(object sender, EventArgs e)
        {
            var tskComplete = new Intent(this, typeof(TaskCompleteView));
            StartActivity(tskComplete);
            Finish();
        }

        private void SetupToolBar()
        {
            SetSupportActionBar(_toolbar);
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);
            SupportActionBar.SetHomeButtonEnabled(true);
            SupportActionBar.SetHomeAsUpIndicator(Resource.Drawable.ic_menu);
           
            
        }

        private void _toolbar_MenuItemClick(object sender, Android.Support.V7.Widget.Toolbar.MenuItemClickEventArgs e)
        {
            throw new NotImplementedException();
        }

        private void GetReference()
        {
            _rvToDoListView = FindViewById<MvxRecyclerView>(Resource.Id.rvToDo);
            _btnAdd = FindViewById<FloatingActionButton>(Resource.Id.btnAdd);
            _toolbar = FindViewById<Android.Support.V7.Widget.Toolbar>(Resource.Id.toolbar);
            _btnCompleteTask = FindViewById<ImageButton>(Resource.Id.btnTaskComplete);
            drawerLayout = FindViewById<DrawerLayout>(Resource.Id.drawer_layout);
            navigationView = FindViewById<NavigationView>(Resource.Id.nav_view);
        }

        private void _btnAdd_Click(object sender, EventArgs e)
        {
            var addintent = new Intent(this, typeof(AddNewTaskView));
            StartActivity(addintent);
        }
    }
}