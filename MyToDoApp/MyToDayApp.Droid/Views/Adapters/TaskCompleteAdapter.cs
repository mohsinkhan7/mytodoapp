﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using MvvmCross.Droid.Support.V7.RecyclerView;
using MvvmCross.Platforms.Android.Binding.BindingContext;
using MyToDayApp.Core.Dto;
using MyToDayApp.Core.Services;
using MyToDayApp.Core.ViewModels;

namespace MyToDayApp.Droid.Views.Adapters
{
    class TaskCompleteAdapter : MvxRecyclerAdapter
    {
        TaskCompleteViewModel _vm;
        Activity _activity;


        public TaskCompleteAdapter(IMvxAndroidBindingContext bindingContext, TaskCompleteViewModel vm, Activity activity) : base(bindingContext)
        {
            _vm = vm;
            _activity = activity;
        }


        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
        {
            var itemBindingContext = new MvxAndroidBindingContext(parent.Context, BindingContext.LayoutInflaterHolder);
            var viewholder = new TaskCompleteAdapterViewHolder(InflateViewForHolder(parent, viewType, itemBindingContext), itemBindingContext, _vm, _activity)
            {

            };

            return viewholder;
        }
        public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
        {
            base.OnBindViewHolder(holder, position);
            var aholder = holder as TaskCompleteAdapterViewHolder;
            aholder.Configure();
        }
        public override long GetItemId(int position)
        {
            return position;
        }
    }

    public class TaskCompleteAdapterViewHolder : MvxRecyclerViewHolder
    {
        TaskCompleteViewModel _vm;
        Activity _activity;
        TextView _txtComplete;
        TextView _txtDate;
        TextView _txtTime;
        KeyDataService _keyDataService = new KeyDataService();

        public TaskCompleteAdapterViewHolder(View itemView, IMvxAndroidBindingContext context, TaskCompleteViewModel vm, Activity activity) : base(itemView, context)
        {
            _vm = vm;
            _activity = activity;

            _txtComplete = itemView.FindViewById<TextView>(Resource.Id.txtComplete);
            _txtDate = itemView.FindViewById<TextView>(Resource.Id.txtDate);
            _txtTime = itemView.FindViewById<TextView>(Resource.Id.txtTime);


        }

        public void Configure()
        {
            var todoDto = (MyToDoDto)DataContext;


            _txtComplete.Text = todoDto.ToDo;

            _txtTime.Text = todoDto.Time;

            _txtDate.Text = todoDto.Date;
            
        }
        
    }
}