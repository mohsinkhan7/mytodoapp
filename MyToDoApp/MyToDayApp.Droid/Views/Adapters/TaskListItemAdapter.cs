﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using MvvmCross.Droid.Support.V7.RecyclerView;
using MvvmCross.Platforms.Android.Binding.BindingContext;
using MyToDayApp.Core.Dto;
using MyToDayApp.Core.Services;
using MyToDayApp.Core.ViewModels;

namespace MyToDayApp.Droid.Views.Adapters
{
    class TaskListItemAdapter : MvxRecyclerAdapter
    {
        TaskListViewModel  _vm;
        Activity _activity;


        public TaskListItemAdapter(IMvxAndroidBindingContext bindingContext, TaskListViewModel  vm, Activity activity) : base(bindingContext)
        {
            _vm = vm;
            _activity = activity;
        }


        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
        {
            var itemBindingContext = new MvxAndroidBindingContext(parent.Context, BindingContext.LayoutInflaterHolder);
            var viewholder = new ToDoListItemAdapterViewHolder(InflateViewForHolder(parent, viewType, itemBindingContext), itemBindingContext, _vm, _activity)
            {
                
            };
            
            return viewholder;
        }
        public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
        {
            base.OnBindViewHolder(holder, position);
            var aholder = holder as ToDoListItemAdapterViewHolder;
            
            aholder.Configure();
        }
        public override long GetItemId(int position)
        {
            return position;
        }
    }

    public class ToDoListItemAdapterViewHolder : MvxRecyclerViewHolder
    {
        TaskListViewModel  _vm;
        Activity _activity;
        TextView _txtToDo;
        public ImageButton _btnUnchk;
        KeyDataService _keyDataService = new KeyDataService();

        public ToDoListItemAdapterViewHolder(View itemView, IMvxAndroidBindingContext context, TaskListViewModel  vm, Activity activity) : base(itemView, context)
        {
            _vm = vm;
            _activity = activity;

            _txtToDo = itemView.FindViewById<TextView>(Resource.Id.txtToDo);
            _btnUnchk = itemView.FindViewById<ImageButton>(Resource.Id.btnunchk);
            
           
        }

        public void Configure()
        {
            var todoDto = (MyToDoDto)DataContext;


            _txtToDo.Text = todoDto.ToDo;
            if(todoDto.IsDone)
            {
                _btnUnchk.SetBackgroundResource(Resource.Drawable.round_checked);
            }
            else
            {
                _btnUnchk.SetBackgroundResource(Resource.Drawable.round_unchecked);
            }
                _btnUnchk.Click += (sender, e) =>
                {
                    if(todoDto.IsDone)
                    {
                        todoDto.IsDone = false;
                        _keyDataService.Save<MyToDoDto>(todoDto.Id, todoDto);
                       
                    }
                    else
                    {
                        todoDto.IsDone = true;
                        var date = DateTime.Now.Date;
                        DateTime d = new DateTime(date.Year, date.Month, date.Day);
                        var dte = d.ToString("dddd, dd MMMM yyyy");
                        todoDto.Date = dte;

                        var time = DateTime.Now;
                        var t = time.ToString("hh:mm tt");
                        todoDto.Time = t;
                        _keyDataService.Save<MyToDoDto>(todoDto.Id, todoDto);
                       
                        
                    }
                    if(todoDto.IsDone)
                    {
                        _btnUnchk.SetBackgroundResource(Resource.Drawable.round_checked);
                    }
                    else
                    {
                        _btnUnchk.SetBackgroundResource(Resource.Drawable.round_unchecked);
                    }

                };


        }
    }
}