﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Views.InputMethods;
using Android.Widget;
using MvvmCross.Droid.Support.V7.AppCompat;
using MvvmCross.Platforms.Android.Views;
using MyToDayApp.Core.Dto;
using MyToDayApp.Core.Services;
using MyToDayApp.Core.ViewModels;

namespace MyToDayApp.Droid.Views
{
    [Activity]
    public class AddNewTaskView :   MvxAppCompatActivity<AddNewTaskViewModel>
    {
        KeyDataService _keyDataService = new KeyDataService();
        private Android.Support.V7.Widget.Toolbar _toolbar;
        RelativeLayout _rlLayout;
        Button _btnSave;
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.add_new_task_view);
            GetReferences();
            SetToolBar();
            

            //_btnSave.Click += _btnSave_Click;
        }

        private void SetToolBar()
        {
            SetSupportActionBar(_toolbar);
        }

        private void GetReferences()
        {
            _btnSave = FindViewById<Button>(Resource.Id.btnsave);
            _toolbar = FindViewById<Android.Support.V7.Widget.Toolbar>(Resource.Id.toolbar);
            _rlLayout = FindViewById<RelativeLayout>(Resource.Id.rlLayout);
        }

        private void _btnSave_Click(object sender, EventArgs e)
        {
            ToSave();
        }
        public void ToSave()
        {
           var intent = new Intent(this, typeof(TaskListView));
            StartActivity(intent);
            Finish();

        }
    }
}