﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using MvvmCross.Platforms.Android.Views;
using MyToDayApp.Core.ViewModels;

namespace MyToDayApp.Droid.Views
{
    [Activity(Label = "Location Photos", MainLauncher = true)]
    public class SigninView:MvxActivity<SigninViewModel>
    {
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.signin_view);
        }
    }
}