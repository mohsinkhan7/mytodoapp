﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using MvvmCross.Droid.Support.V7.AppCompat;
using MvvmCross.Droid.Support.V7.RecyclerView;
using MvvmCross.Platforms.Android.Binding.BindingContext;
using MvvmCross.Platforms.Android.Views;
using MyToDayApp.Core.ViewModels;
using MyToDayApp.Droid.Views.Adapters;

namespace MyToDayApp.Droid.Views
{
    [Activity]
    public class TaskCompleteView : MvxAppCompatActivity<TaskCompleteViewModel>
    {
        public MvxRecyclerView _rvTaskComplete;
        private Android.Support.V7.Widget.Toolbar _toolbar;
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.task_complete_view);
            GetReferences();
            SetActionBar();
            var adapter = new TaskCompleteAdapter((IMvxAndroidBindingContext)BindingContext, this.ViewModel, this);
            _rvTaskComplete.Adapter = adapter;
            ViewModel.GetCompleteTaskData();
            adapter.NotifyDataSetChanged();
        }

        private void SetActionBar()
        {
            SetSupportActionBar(_toolbar);
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);
            SupportActionBar.SetHomeButtonEnabled(true);
            SupportActionBar.Title = "Task Completed";
          
        }

        private void GetReferences()
        {
            _rvTaskComplete = FindViewById<MvxRecyclerView>(Resource.Id.rvTaskComplete);
            _toolbar = FindViewById<Android.Support.V7.Widget.Toolbar>(Resource.Id.toolbar);
        }
    }
}