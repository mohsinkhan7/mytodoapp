﻿using MvvmCross.ViewModels;
using MyToDayApp.Core.Dto;
using MyToDayApp.Core.Services;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MyToDayApp.Core.ViewModels
{
    public class TaskCompleteViewModel: MvxViewModel
    {
        readonly IKeyDataService _keydataService;
        MyToDoDto myToDoDto = new MyToDoDto();
        public TaskCompleteViewModel(IKeyDataService keydataService)
        {
            _keydataService = keydataService;
        }
        public override async Task Initialize()
        {
            await base.Initialize();
        }
        private List<MyToDoDto> _completeList = new List<MyToDoDto>();

        public List<MyToDoDto> CompleteList
        {
            get { return _completeList; }
            set { _completeList = value;
                RaisePropertyChanged(() => CompleteList);
            }
        }

      
        public void GetCompleteTaskData()
        {
            
            foreach (var item in _keydataService.GetAll())
            {
               var value = JsonConvert.DeserializeObject<MyToDoDto>(item.Value);
                if(value.IsDone)
                {
                    CompleteList.Add(JsonConvert.DeserializeObject<MyToDoDto>(item.Value));
                }


               
            }
        }
    }
}
