﻿using MvvmCross.Commands;
using MvvmCross.Navigation;
using MvvmCross.ViewModels;
using MyToDayApp.Core.Dto;
using MyToDayApp.Core.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace MyToDayApp.Core.ViewModels
{
    public class SigninViewModel : MvxViewModel
    {
        readonly IMvxNavigationService _navigationservice;
        readonly IKeyDataService _keydataService;
        UserDto userDto = new UserDto();
        public SigninViewModel(IMvxNavigationService navigationservice,IKeyDataService keydataService)
        {
            _navigationservice = navigationservice;
            _keydataService = keydataService;
        }
        public override async Task Initialize()
        {
            await base.Initialize();
        }
        private MvxCommand _register;
        public ICommand RegisterCommand
        {
            get
            {

                return new MvxCommand(DoRegisterNavigate);
            }

        }
        public void DoRegisterNavigate()
        {
            _navigationservice.Navigate<RegisterViewModel>();
        }
        private string _username;

        public string UserName
        {
            get { return _username; }
            set { _username = value; }
        }
        private string password;

        public string Password
        {
            get { return password; }
            set { password = value; }
        }
        public ICommand LoginCommand
        {
            
            get
            {
              return new MvxCommand(DoLogin);
                
            }

        }
        public void DoLogin()
        {
            _navigationservice.Navigate<TaskListViewModel >();
        }
    }
}
