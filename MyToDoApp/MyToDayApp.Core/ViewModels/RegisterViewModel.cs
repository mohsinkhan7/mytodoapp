﻿using MvvmCross.Commands;
using MvvmCross.Navigation;
using MvvmCross.ViewModels;
using MyToDayApp.Core.Dto;
using MyToDayApp.Core.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace MyToDayApp.Core.ViewModels
{
    public class RegisterViewModel: MvxViewModel
    {
        readonly IKeyDataService _keydataService;

        readonly IMvxNavigationService _navigationService;
        UserDto userDto = new UserDto();
        public RegisterViewModel(IMvxNavigationService navigationService, IKeyDataService keydataService)
        {
            _navigationService = navigationService;
            _keydataService = keydataService;
        }
      
        private string _username;

        public string UserName
        {
            get { return _username; }
            set { _username = value; }
        }
        private string password;

        public string Password
        {
            get { return password; }
            set { password = value; }
        }
        
        public ICommand SaveCommand
        {
            get
            {
               
                return new MvxCommand(SaveRegister); 
                
                
            }
        }
        public void SaveRegister()
        {
            _navigationService.Navigate<SigninViewModel>();
        }

       
    }
}
