﻿using MvvmCross.Commands;
using MvvmCross.Navigation;
using MvvmCross.ViewModels;
using MyToDayApp.Core.Dto;
using MyToDayApp.Core.Services;
using Realms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace MyToDayApp.Core.ViewModels
{
    public class AddNewTaskViewModel : MvxViewModel<string, MyToDoDto>
    {
        readonly IKeyDataService _keydataService;
        MyToDoDto MyToDoDto = new MyToDoDto();
        readonly IMvxNavigationService _navigationservice;
        public List<KeyData> mylist;
        public AddNewTaskViewModel(IMvxNavigationService navigationservice, IKeyDataService keydataService)
        {
            _navigationservice = navigationservice;
            _keydataService = keydataService;
            
        }
        public override async Task Initialize()
        {
            await base.Initialize();
        }
        private int _id;

        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private string _todoText;

        public string TodoText
        {
            get { return _todoText; }
            set { _todoText = value; }
        }

        private MvxCommand _saveCommand;

        public ICommand SaveCommand
        {
            get { return _saveCommand == null ? new MvxCommand(DoSaveCommand) : _saveCommand; }
        }

        private void DoSaveCommand()
        {

            MyToDoDto mtd = new MyToDoDto() { ToDo = TodoText, IsDone = false };
            _keydataService.Save<MyToDoDto>(mtd.Id, mtd);
            _navigationservice.Close(this, mtd);
        }

        public override void Prepare(string parameter)
        {
        }
    }
}
