﻿using Android.OS;
using MvvmCross.Commands;
using MvvmCross.Navigation;
using MvvmCross.ViewModels;
using MyToDayApp.Core.Dto;
using MyToDayApp.Core.Services;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace MyToDayApp.Core.ViewModels
{
    public class TaskListViewModel : MvxViewModel
    {
        
        readonly IKeyDataService _keydataService;
        private readonly IMvxNavigationService _mvxNavigationService;
        public TaskListViewModel (IKeyDataService keydataService, IMvxNavigationService mvxNavigationService)
        {
            _keydataService = keydataService;
            FilteredList = MytodoList;
            _mvxNavigationService = mvxNavigationService;
            //NavigateCommand =new MvxAsyncCommand(() => _mvxNavigationService.Navigate<AddNewTaskViewModel>());
        }
              
        public override async Task Initialize()
        {
            await base.Initialize();
        }
        private List<MyToDoDto> _mytodoList = new List<MyToDoDto>();

        public List<MyToDoDto> MytodoList
        {
            get { return _mytodoList; }
            set
            {
                _mytodoList = value;
                RaisePropertyChanged(() => MytodoList);
            }
        }
        private string _searchString;

        public string Query
        {
            get { return _searchString; }
            set
            {
                _searchString = value;
                RaisePropertyChanged(() => Query);
            }
        }

        public void SearchQuery(string query)
        {
            Query = query;
            FilterList( Query);
        }
        
        private void FilterList( string query)
        {
           
            if (string.IsNullOrEmpty(Query))
            {
                FilteredList = MytodoList;
            }
            else FilteredList = MytodoList.Where(c => c.ToDo.ToLower().Contains(query)).ToList();
        }
        private List<MyToDoDto> _filteredlist;

        public List<MyToDoDto> FilteredList
        {
            get { return _filteredlist; }
            set
            {
                _filteredlist = value;
                RaisePropertyChanged(() => FilteredList);
            }
        }
        //private IMvxAsyncCommand _navigate;

        //public IMvxAsyncCommand NavigateCommand
        //{
        //    get { return _navigate; }
        //    set { _navigate = value; }
        //}

        private MvxCommand _newTaskCommand;
        public ICommand NewTaskCommand
        {
            get { return _newTaskCommand == null ? new MvxCommand(DoNewTaskCommand) : _newTaskCommand; }
            //get { return _newTaskCommand ?? new MvxCommand(DoNewTaskCommand); }
        }

        private async void DoNewTaskCommand()
        {
            var output = await _mvxNavigationService.Navigate<AddNewTaskViewModel, string, MyToDoDto>("test input");

            MytodoList.Add(output);
            RaisePropertyChanged(() => MytodoList);
        }

        public void GetData()
        {
            //MytodoList.Clear();
            foreach (var item in _keydataService.GetAll())
            {
                MytodoList.Add(JsonConvert.DeserializeObject<MyToDoDto>(item.Value));
            }
        }     
    }
}
