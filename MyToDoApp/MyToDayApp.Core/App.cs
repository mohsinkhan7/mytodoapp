﻿using MvvmCross;
using MvvmCross.ViewModels;
using MyToDayApp.Core.Services;
using MyToDayApp.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace MyToDayApp.Core
{
    public class App : MvxApplication
    {
        public override void Initialize()
        {
            Mvx.IoCProvider.RegisterType<IKeyDataService, KeyDataService>();

            RegisterAppStart<SigninViewModel>();
        }
    }
}
