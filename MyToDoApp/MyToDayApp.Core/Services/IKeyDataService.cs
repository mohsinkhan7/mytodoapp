using MyToDayApp.Core.Dto;
using System.Collections.Generic;

namespace MyToDayApp.Core.Services
{
    public interface IKeyDataService
    {
        T Find<T>(string key);

        bool Save<T>(string key, T value);

        bool Delete(string key);

        List<KeyData> GetAll();
    }
}
