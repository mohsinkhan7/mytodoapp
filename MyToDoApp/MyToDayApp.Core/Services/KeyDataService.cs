
using Realms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using MyToDayApp.Core.Dto;

namespace MyToDayApp.Core.Services
{
    public class KeyDataService : IKeyDataService
    {
        private RealmConfiguration _realmConfiguration;

        public KeyDataService()
        {
            _realmConfiguration = new RealmConfiguration
            {
                SchemaVersion = 1
            };
        }

        public T Find<T>(string key)
        {
            try
            {
                var realm = Realms.Realm.GetInstance(_realmConfiguration);

                var obj = realm.Find<KeyData>(key);

                if (obj == null)
                {
                    return default(T);
                }

                var res = JsonConvert.DeserializeObject<T>(obj.Value);

                return res;
            }
            catch (Exception e)
            {
                return default(T);
            }
        }

        public bool Delete(string key)
        {
            try
            {
                var realm = Realms.Realm.GetInstance(_realmConfiguration);

                var obj = realm.Find<KeyData>(key);

                if (obj == null)
                {
                    return false;
                }

                //using (var trans = realm.BeginWrite())
                //{
                //    realm.Remove(obj);
                //    trans.Commit();
                //}

                realm.Write(() =>
                {
                    realm.Remove(obj);
                });

                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public bool Save<T>(string key, T value)
        {
            try
            {
                var realm = Realms.Realm.GetInstance(_realmConfiguration);
                var val = JsonConvert.SerializeObject(value);

                var obj = realm.Find<KeyData>(key);

                realm.Write(() =>
                {
                    if (obj == null)
                    {
                        obj = new KeyData();
                        obj.Key = key;
                    }

                    obj.Value = val;

                    realm.Add(obj, update: true);
                });

                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public List<KeyData> GetAll()
        {
            var realm = Realms.Realm.GetInstance(_realmConfiguration);
            return realm.All<KeyData>().ToList();
        }
       
    }
}
