﻿using Realms;
using System;
using System.Collections.Generic;
using System.Text;

namespace MyToDayApp.Core.Dto

{
    public class KeyData : RealmObject
    {
        [PrimaryKey]
        public string Key { get; set; }
        public string Value { get; set; }
    }
}
