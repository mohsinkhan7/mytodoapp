﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MyToDayApp.Core.Dto
{
    public class MyToDoDto 
    {
        public string Id { get; set; } = Guid.NewGuid().ToString();

        public string ToDo { get; set; }
        public bool IsDone { get; set; }
        public string Date { get; set; }
        public string Time { get; set; }

    }
}
