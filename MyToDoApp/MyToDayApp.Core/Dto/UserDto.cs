﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MyToDayApp.Core.Dto
{
    public class UserDto
    {
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}
